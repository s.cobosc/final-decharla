import requests
import json
from xml.dom import minidom
import os

LOCATION_CODE = ''

URL = "https://opendata.aemet.es"

id_sesena = 'id45161'

AEMET_API_KEY = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtbWdnMzY1NDhAZ21haWwuY29tIiwianRpIjoiZWU0ZjFmYmUtODc2Zi00M2RhLWExZTgtM2FhYjJlNDk3Mzg5IiwiaXNzIjoiQUVNRVQiLCJpYXQiOjE2ODM2NTYxOTQsInVzZXJJZCI6ImVlNGYxZmJlLTg3NmYtNDNkYS1hMWU4LTNhYWIyZTQ5NzM4OSIsInJvbGUiOiIifQ.uXNt1B_deiFWxbbCBDX0Voyztpj97e4grS__YH6Bna4'
querystring = { "api_key": AEMET_API_KEY }
headers = {
    'cache-control': "no-cache",
    'accept': 'application/json'
}

def save_messages(data_xml):
    keeptalking_url = 'http://mainppgg.pythonanywhere.com'
    keeptalking_path = '/chatroom/hoawyvetcz/messages'

    headers = { 'Authorization': "Basic 1234", "content-type": "application/xml" }

    response = requests.request("PUT", keeptalking_url + keeptalking_path, data=data_xml, headers=headers)
    print(response)

def create_xml(data):
    root = minidom.Document()
  
    messages = root.createElement('messages') 
    root.appendChild(messages)
    
    for m in data:
        #loop
        message = root.createElement('message')
        message.setAttribute('isimg', 'false')

        text = root.createElement('text')
        text.appendChild(root.createTextNode(m))
        
        message.appendChild(text)
        messages.appendChild(message)
    # end loop
    
    xml_str = root.toprettyxml(indent ="\t", encoding='utf-8')
    # encoding="UTF-8"

    return xml_str
    
    # TODO: save data xml in external file
    # save_path_file = "api/test.xml"
    # with open(save_path_file, "w") as f:
    #     f.write(xml_str)

def filter_data(data_arr):
    data = data_arr[0]

    message_list = list()
    for day in data['prediccion']['dia']:
        message_str = ''
        message_str += 'Origen: ' + data['origen']['productor'] + '\n'
        message_str += 'Localidad: ' + data['nombre'] + '\n'
        message_str += 'Provincia: ' + data['provincia'] + '\n'

        message_str += 'Fecha: ' + day['fecha'] + '\n'
        message_str += 'Temp maxima: ' + str(day['temperatura']['maxima']) + '\n'
        message_str += 'Temp minima: ' + str(day['temperatura']['minima']) + '\n'
        message_list.append(message_str)

    return message_list

def get_municipios():
    path = '/opendata/api/maestro/municipios/'

    response = requests.request("GET", URL + path, headers=headers, params=querystring)
    response_data = response.json()
    nlist = list()
    for item in response_data:
        nlist.append({ item['nombre']: item['id']})

    f = open('api/municipios.json', 'w')
    f.write(str(nlist))
    # f.write(json.dumps(response.json()))
    f.close()

def get_time_aemet_by_location(location_id):
    
    path = '/opendata/api/prediccion/especifica/municipio/diaria/' + location_id

    response = requests.request("GET", URL + path, headers=headers, params=querystring)
    response_json = response.json()
    if response.status_code == 200:
        url_datos = response_json['datos']
        response_datos = requests.request("GET", url_datos, headers=headers, params=querystring)
        return response_datos.json()

if __name__ == '__main__':
    # https://opendata.aemet.es/dist/index.html#/predicciones-especificas/Predicción%20de%20montaña.%20Tiempo%20pasado.
    # get_municipios()
    location_id = '45161' # seseña
    location_data = get_time_aemet_by_location(location_id)
    location_data_filtered = filter_data(location_data)
    xml_str = create_xml(location_data_filtered)

    save_messages(xml_str)

    # create_xml()
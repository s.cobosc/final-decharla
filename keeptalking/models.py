from django.db import models

class Session(models.Model):
    name = models.CharField(max_length=100, default=None, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)

class Authorization(models.Model):
    password = models.CharField(max_length=100, default=None, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
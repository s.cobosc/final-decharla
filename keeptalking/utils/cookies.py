from Crypto.Cipher import AES
from base64 import b64encode, b64decode
import json

class LoggedCookie(object):
    def __init__(self, name, font_family, font_size, session_id):
        self.name = name
        self.font_family = font_family
        self.font_size = font_size
        self.session_id = session_id

class AESCipher(object):
    def __init__(self, salt='SlTKeYOpHygTYkP3'):
        self.salt = salt.encode('utf8')
        self.enc_dec_method = 'utf-8'

    def encrypt(self, str_to_enc, str_key):
        try:
            aes_obj = AES.new(str_key.encode('utf-8'), AES.MODE_CFB, self.salt)
            hx_enc = aes_obj.encrypt(str_to_enc.encode('utf8'))
            mret = b64encode(hx_enc).decode(self.enc_dec_method)
            return mret
        except ValueError as value_error:
            raise ValueError(value_error)

    def decrypt(self, enc_str, str_key):
        try:
            aes_obj = AES.new(str_key.encode('utf8'), AES.MODE_CFB, self.salt)
            str_tmp = b64decode(enc_str.encode(self.enc_dec_method))
            str_dec = aes_obj.decrypt(str_tmp)
            mret = str_dec.decode(self.enc_dec_method)
            return mret
        except ValueError as value_error:
            raise ValueError(value_error)
    
def create_cookie(session_id):
    key = '1234123412341234'
    # TODO: extract pass to real password out of gitlab
    cryptor = AESCipher()
    new_cookie = LoggedCookie('anonimous', None, None, session_id)
    return cryptor.encrypt(json.dumps(new_cookie.__dict__), key)

def read_cookie(encode_text):
    key = '1234123412341234'
    cryptor = AESCipher()
    return cryptor.decrypt(encode_text, key)

def encrypt_cookie(loggedCookie: LoggedCookie):
    key = '1234123412341234'
    cryptor = AESCipher()
    return cryptor.encrypt(json.dumps(loggedCookie.__dict__), key)
from chatroom.models import *

def get_metrics():
    metrics = {}

    metrics['active_chatrooms'] = ChatRoom.objects.all().exclude(chatroommessage=None).count()
    metrics['text_messages'] = ChatRoomMessage.objects.filter(is_image=False).count()
    metrics['image_messages'] = ChatRoomMessage.objects.filter(is_image=True).count()

    return metrics
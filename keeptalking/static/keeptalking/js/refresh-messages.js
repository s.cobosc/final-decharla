
function makeMessagesTemplate (message) {
  return `
    <div class="message__container ${
      message.self_message ? 'self__message' : ''
    }">
      <div class="message__title">
        <!-- author -->
        ${message.session ? message.session.name : 'BOT'}
      </div>

      ${
        message.is_image == false ?
        `<div class="message__description">
          ${message.description}
        </div>`:
        `<div class="message__description is_image">
          <img src="${message.image_url}" alt="img" id="${message.id}" onclick="zoom('${message.id}')">
        </div>`
      }
    </div>
  `
}



function getMessages () {
  console.log(window.location.href)

  const messagesItems = document.getElementById("chatroom-messages")

  fetch(window.location.href + '/messages' + '?' + new URLSearchParams({
      format: 'json',
  })).then(response => {
    return response.json()
  }).then(json => {
    var messages = json.messages
    inner = ``
    console.log(messages)
    messages.reverse().forEach(element => {
      inner += makeMessagesTemplate(element)
    });
    console.log(inner)
    messagesItems.innerHTML = inner
    messagesItems.scrollTo(0, messagesItems.scrollHeight)

  }).catch(function (e) {
    console.log("Error", e)
  })
}

const sw_dinamic = document.getElementById("switch-dinamic");

var is_dinamic = false

sw_dinamic.addEventListener('change', () => {
  is_dinamic = !is_dinamic
  console.log(is_dinamic)
  if (is_dinamic) {
    timer = window.setInterval(() => {
      console.log("Interval");
      getMessages();
    }, 3 * 1000); // Call every 3 seconds
  } else {
    if (timer) {
      window.clearInterval(timer);
      timer = null
    }
  }
});

window.addEventListener("DOMContentLoaded", () => {
    console.log(document.location.href)
    // if (document.location.href.includes('/chatroom/') && is_dinamic == true) {
    //     setInterval(() => {
    //         console.log("Interval");
    //         getMessages();
    //     }, 3 * 1000); // Call every 3 seconds
    // }
});
  
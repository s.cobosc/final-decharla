// search chatrooms

function makeTemplate (chatroom) {
  console.log(chatroom)
  return `
    <div class="home__bar--item">
      <a href="/chatroom/${chatroom.code}">
        <span class="goto__chatroom material-symbols-outlined">
          arrow_forward
        </span>
      </a>
      <span class="text">${chatroom.name}</span>
    </div>
  `
}

function makeNoResultsTemplate () {
return `
    <div class="home__bar--item no__results">
      <span class="icon-warning material-symbols-outlined">
        warning
      </span>
      <span class="text">No chatrooms for that search...</span>
    </div>
  `
}

const url = window.location.href + 'chatroom'

function searchChatrooms () {
  console.log(window.location.href)
  const search = document.getElementById("chatroom_search").value

  const homeItems = document.getElementById("home-items")

  fetch(url + '?' + new URLSearchParams({
      search: search,
  })).then(response => {
    console.log("Response received!", response)
    if (!response.ok) {
      throw new Error("HTTP error " + response.status);
    }
    return response.json();
  }).then(json => {
    console.log(json);
    const chatrooms = JSON.parse(json);
    console.log(json.length)
    if (chatrooms.length == 0) {
      homeItems.innerHTML = makeNoResultsTemplate()
      return
    }
    inner = ``
    chatrooms.forEach(element => {
      console.log(element.model)
      inner += makeTemplate(element.fields)
    });
    homeItems.innerHTML = inner
  }).catch(function () {
    console.log("Error decoding JSON")
  })
}

// Get the input field
var input = document.getElementById("chatroom_search");

// Execute a function when the user presses a key on the keyboard
// input.addEventListener("keypress", function(event) {
//   console.log('cickk')
//   // If the user presses the "Enter" key on the keyboard
//   if (event.key === "Enter") {
//     // Cancel the default action, if needed
//     event.preventDefault();
//     // Trigger the button element with a click
//     if (input != '') {
//       searchChatrooms();
//     }
//   }
// });
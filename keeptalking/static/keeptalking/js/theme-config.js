const sw = document.getElementById("switch");

sw.addEventListener('change', () => {
  let theme = localStorage.getItem('theme-appearance');
  console.log(theme)
  if (theme === 'dark'){
    toLight()
  }else{
    toDark()
  }
});

let theme = localStorage.getItem('theme-appearance');

console.log(theme)

if(theme === 'dark'){
  sw.checked = true
  toDark()
}

function toDark() {
  localStorage.setItem('theme-appearance', 'dark')
  document.documentElement.setAttribute('data-theme', 'dark')
}

function toLight() {
  localStorage.setItem('theme-appearance', 'light')
  document.documentElement.setAttribute('data-theme', 'light')
}
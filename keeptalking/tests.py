from django.test import TestCase
from keeptalking.models import *
from django.utils import timezone
from django.urls import reverse
from keeptalking.utils import cookies
from http.cookies import SimpleCookie
from chatroom.models import *
import json
from django.core.exceptions import ObjectDoesNotExist

# Test for models app keeptalking -> password

class TestKeepTalking(TestCase):
    def create_chatroom(self, name="test chatroom name", description = "test description", code="asdf2949sDD"):
        return ChatRoom.objects.create(name=name, created_at=timezone.now(), description=description, code=code)
    
    def create_session(self, name="test name"):
        return Session.objects.create(name=name, created_at=timezone.now())
    
    def create_cookie_with_session(self, name="test with cookie"):
        session_test =  Session.objects.create(name=name, created_at=timezone.now())
        return cookies.create_cookie(session_test.id)
    
    def test_session_creation(self):
        session = self.create_session()
        self.assertTrue(isinstance(session, Session))
        self.assertEqual(session.name, "test name")

    def test_status_login(self):
        # 100% available -> 200 ok
        session = self.create_session()
        resp = self.client.get('/login')
        self.assertEqual(resp.status_code, 200)

    def test_status_home(self):
        # no cookies -> err 302
        session = self.create_session()
        resp = self.client.get('/')
        
        self.assertEqual(resp.status_code, 302)

        # same with cookie
        cookie = self.create_cookie_with_session()
        self.client.cookies = SimpleCookie({'JSESSIONID': cookie})
        resp_auth = self.client.get('/')

        self.assertEqual(resp_auth.status_code, 200)

    def test_create_chatroom(self):
        # no cookies -> err 302
        resp = self.client.post('/')
        self.assertEqual(resp.status_code, 302)

        # same with cookie
        cookie = self.create_cookie_with_session()        
        payload = {
            'name': 'name',
            'description': 'description'
        }
        self.client.cookies = SimpleCookie({'JSESSIONID': cookie})
        resp_auth = self.client.post('/', payload, format='json')
        
        chat_code_created = resp_auth.url.replace('/chatroom/', '')

        try:
            c = ChatRoom.objects.get(code='asdasdfff')
        except ChatRoom.DoesNotExist:
            c = None

        self.assertEqual(ChatRoom.objects.get(code=chat_code_created).code, chat_code_created)

        self.assertRaises(ChatRoom.DoesNotExist)
        self.assertEqual(resp_auth.status_code, 302)

    def test_status_config(self):
        # no cookies -> err 302
        resp = self.client.get('/config')
        
        self.assertEqual(resp.status_code, 302)

        # same with cookie
        cookie = self.create_cookie_with_session()
        self.client.cookies = SimpleCookie({'JSESSIONID': cookie})
        resp_auth = self.client.get('/config')

        self.assertEqual(resp_auth.status_code, 200)

    def test_status_help(self):
        # no cookies -> err 302
        resp = self.client.get('/help')
        
        self.assertEqual(resp.status_code, 302)

        # same with cookie
        cookie = self.create_cookie_with_session()
        self.client.cookies = SimpleCookie({'JSESSIONID': cookie})
        resp_auth = self.client.get('/help')

        self.assertEqual(resp_auth.status_code, 200)

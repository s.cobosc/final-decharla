from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from chatroom.models import *
from django.views.decorators.csrf import csrf_exempt
from .utils import cookies, metrics as m_utils
import json
import random
import string


def generate_rand_code(length):

    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


@csrf_exempt
def home(request):
    if request.method == 'GET':
        chatrooms = ChatRoom.objects.all()
        for chatroom in chatrooms:
            count = ChatRoomMessage.objects.filter(chatroom=chatroom).count()
            chatroom.count = count
            chatroom.likes = Vote.objects.filter(
                chatroom=chatroom, status=True).count()
            chatroom.dislikes = Vote.objects.filter(
                chatroom=chatroom, status=False).count()

        template = loader.get_template('keeptalking/home.html')
        return HttpResponse(template.render({'chatrooms': chatrooms, 'css_properties': request.css_properties, 'metrics': request.metrics}))
    if request.method == 'POST':
        # CREATE CHATROOM
        chatroom = ChatRoom(name=request.POST['name'], description=request.POST['description'],
                            code=generate_rand_code(10))
        chatroom.save()
        return HttpResponseRedirect('/chatroom/' + chatroom.code)


@csrf_exempt
def login(request):
    if request.method == 'GET':
        template = loader.get_template('keeptalking/ChatroomLogin.html')
        return HttpResponse(template.render({'isLogged': True}))

    if request.method == 'POST':

        if not 'password' in request.POST:
            return HttpResponse('NOT FOUND')
        
        pwd = request.POST['password']
        try:
            Authorization.objects.get(password=pwd)
        except Authorization.DoesNotExist:
            errors = {}
            errors['status_error'] = 'Incorrect password, try again'
            template = loader.get_template('keeptalking/ChatroomLogin.html')
            return HttpResponse(template.render({'isLogged': True, 'errors': errors}))
        # if not Authorization.objects.exists(password=pwd):
        #     return HttpResponseBadRequest('err auth')

        response = HttpResponseRedirect('/')

        # create new session
        session = Session()
        session.save()

        response.set_cookie('JSESSIONID', cookies.create_cookie(session.id))
        return response


@csrf_exempt
def config(request):
    dict(name='default', value='')
    form = [{'name': 'default', 'value': ''}, {'name': 'Segoe UI', 'value': 'font__family-1'},
            {'name': 'Courier New', 'value': 'font__family-2'}, {'name': 'Verdana', 'value': 'font__family-3'}]
    if request.method == 'GET':
        template = loader.get_template('keeptalking/config.html')
        return HttpResponse(template.render({'css_properties': request.css_properties, 'form': form, 'metrics': request.metrics}))

    if request.method == 'POST':
        template = loader.get_template('keeptalking/config.html')

        name = request.POST['name'] if request.POST['name'] != '' else None
        modifiedCookie = cookies.LoggedCookie(name,
                                              request.POST['font_family'] if request.POST['font_family'] != '' else None,
                                              request.POST['font_size'] if request.POST['font_size'] != '' else None,
                                              request.css_properties['session_id'])

        session = Session.objects.get(id=request.css_properties['session_id'])
        session.name = name
        session.save()

        response = HttpResponse(template.render(
            {'css_properties': modifiedCookie.__dict__, 'form': form}))
        response.set_cookie(
            'JSESSIONID', cookies.encrypt_cookie(modifiedCookie))
        return response


@csrf_exempt
def help(request):
    if request.method == 'GET':
        template = loader.get_template('keeptalking/help.html')
        return HttpResponse(template.render({'css_properties': request.css_properties, 'metrics': request.metrics}))
    else:
        # bad request
        return HttpResponse('NOT FOUND')


@csrf_exempt
def logout(request):
    if request.method == 'GET':
        response = HttpResponseRedirect('/')
        response.delete_cookie('JSESSIONID')
        return response
    else:
        # bad request
        return HttpResponse('NOT FOUND')

from django.apps import AppConfig


class KeeptalkingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'keeptalking'

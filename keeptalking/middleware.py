from django.http import HttpResponse, HttpResponseRedirect
from .utils import cookies, metrics
import json

BASIC_PASS = '1234'

def is_auth(auth):
    auth_check = auth.replace('Basic ', '')
    return auth_check == BASIC_PASS

def cookie_middleware(get_response):
    def middleware(request):
        if request.path == '/login':
            return get_response(request)
        if request.method == 'PUT' and request.META.get('HTTP_AUTHORIZATION'):
            print(request.META['HTTP_AUTHORIZATION'])
            if is_auth(request.META['HTTP_AUTHORIZATION']):
                return get_response(request)
            else:
                return HttpResponse('Unauthorized', status=401)
        if request.COOKIES.get('JSESSIONID') != None:
            request.css_properties = json.loads(cookies.read_cookie(request.COOKIES.get('JSESSIONID')))
            request.metrics = metrics.get_metrics()
            print(request.css_properties)
            return get_response(request)
        else:
            print(request.path)
            request.css_properties = None

        return HttpResponseRedirect('/login')

    return middleware
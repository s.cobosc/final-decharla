from django.contrib import admin

from .models import Session, Authorization

admin.site.register(Session)
admin.site.register(Authorization)

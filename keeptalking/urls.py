from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("login", views.login, name="login"),
    path("config", views.config, name="config"),
    path("help", views.help, name="help"),
    path("logout", views.logout, name="logout")
]
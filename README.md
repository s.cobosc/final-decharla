# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Samuel Cobos Correa
* Titulación: Ingeniería en tecnologías de la Telecomunicación
* Cuenta en laboratorios: scorrea
* Cuenta URJC: s.cobosc
* Video básico (url): https://youtu.be/Sfxsl4suBTY
* Video parte opcional (url): https://youtu.be/o3jQTfaD_QU
* Despliegue (url): https://mainppgg.pythonanywhere.com/
* Contraseñas: 1234, hola
* Cuenta Admin Site: main/1111

## Resumen parte obligatoria

Aplicación con Django para disponer salas de chat, renombrado a Keeptaking (deCharla). La aplicación tiene las siguiente funcionalidades:
* Página de login "/login" donde disponemos de un formulario para acceder a la aplicación, en caso de introducir una contraseña incorrecta devuelve un error. De lo contrario, accederemos a la pagina principal "/" de la app.

* Página principal que dispondremos de varios apartados:
    - Header con el logo de la aplicación un tooltip en el nombre de la sesión que dispondrá de varios apartados, entre ellos la funcionalidad de cambiar el tema de la aplicación
    - Contenido principal donde se mostrará el contenido de las diferentes páginas.
    - En el contenido principal se mostrarán las salas activas
    - Aside bar, con el buscador de salas, devolverá un objeto JSON de respuesta para obtener las salas que coincidan con el nombre buscado.
    - Footer con las métricas correspondientes.
    Cabe destacar que Header y Footer lo heredarán todos los apartados de la aplicación.

* Página de configuración para configurar el tipo de letra, el tamaño, y el nombre a mostrar de la sesión.

* Página de cada sala:
    - Con una lista de mensajes ordenados por id, a la inversa para asociar el contenido a una típica sala de chat (de abajo a arriba)

* Página dinámica que la encontraremos en la página normal de una sala, con un tooltip activando asi el dinamic dentro de un js


## Lista partes opcionales

Se incluyen TODAS las partes opcionales, todas ellas reflejadas a lo largo de la app:

* Favicon.
* Dentro del tooltip en el header, tendremos la opción de salir de la sesión redireccionando a la pantalla de Login.
* En la esquina superior derecha de la página de la sala, se incluye una sección en la que puedes votar en una sala (tanto like como dislike). Solo se podrá votar una vez por sesion y por sala.
* Se optimizaron todos los test y se añadieron mas.
* script de funcionalidad se puede encontrar en la carpeta api, que es una api externa para comunicarse con AEMET, transformarlo en un XML, y nuevamente hacer un put a nuestra aplicación para insertar estos mensajes leyendo el XML con lo aprendido en clase. Creación con minidom y con sax el parser.
* El apartado anterior se incluyó también en la app y dentro de la sección de cada sala de chat, aparece el boton en la esquina inferior izquierda para añadir mensajes con un formulario, indicando una población.


# Other

# first app commands

- django-admin startproject { projectName }

- python manage.py startapp { keeptalking }

- enjoy... life is short

# Migrations and configure installed apps commands

- python manage.py migrate

any change on model to create migrations... new models and stuff

- python manage.py makemigrations keeptalking

show db query migration

- python manage.py sqlmigrate keeptalking { numberOfMigration }

# User and how to use admin site

- python manage.py createsuperuser

chip palette https://palettes.shecodes.io/palettes/50

# Article CSS HTML native chat messages component

- https://ishadeed.com/article/facebook-messenger-chat-component/


# pwds

- 1234
- hola
from django.test import TestCase
from keeptalking.models import *
from django.utils import timezone
from django.urls import reverse
from keeptalking.utils import cookies
from http.cookies import SimpleCookie
from chatroom.models import *

# Test for models app keeptalking -> password

class TestLogin(TestCase):
    def create_chatroom(self, name="test chatroom name", description = "test description", code="asdf2949sDD"):
        return ChatRoom.objects.create(name=name, created_at=timezone.now(), description=description, code=code)
    
    def create_session(self, name="test name"):
        return Session.objects.create(name=name, created_at=timezone.now())
    
    def create_chatroom_message(self, description="des", is_image=False, image_url=None, chatroom=None, session=None):
        if chatroom is None:
            chatroom = self.create_chatroom()
        if session is None:
            session = self.create_session()
        return ChatRoomMessage.objects.create(description=description,
                                              is_image=is_image,
                                              image_url=image_url,
                                              chatroom=chatroom,
                                              session=session)
    
    def create_vote(self, status=True, chatroom=None, session=None):
        if chatroom is None:
            chatroom = self.create_chatroom()
        if session is None:
            session = self.create_session()
        return Vote.objects.create(status=status, chatroom=chatroom, session=session)
    
    def create_cookie_with_session(self, name="test with cookie"):
        session_test =  Session.objects.create(name=name, created_at=timezone.now())
        return cookies.create_cookie(session_test.id)
    
    def test_chatroom_creation(self):
        chatroom = self.create_chatroom()
        self.assertTrue(isinstance(chatroom, ChatRoom))
        self.assertEqual(chatroom.name, "test chatroom name")
        self.assertEqual(chatroom.description, "test description")

    def test_status_chatroom(self):
        # no cookies -> err 302
        resp = self.client.get('/chatroom')

        self.assertEqual(resp.status_code, 302)
        
        # same with cookie
        # TODO
        cookie = self.create_cookie_with_session()
        self.client.cookies = SimpleCookie({'JSESSIONID': cookie})

        chatroom = self.create_chatroom()
        resp_auth = self.client.get('/chatroom/' + chatroom.code)

        self.assertEqual(resp_auth.status_code, 200)
    
    def test_chatroom_message_creation(self):
        msg = self.create_chatroom_message()

        self.assertIsInstance(msg, ChatRoomMessage)
        self.assertEqual(msg.description, "des")

    def test_vote_creation(self):
        vote = self.create_vote()

        self.assertIsInstance(vote, Vote)
        self.assertTrue(vote.status)
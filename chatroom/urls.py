from django.urls import path

from . import views

urlpatterns = [
    path("<str:code>", views.chatroom, name="chatroom"),
    path("<str:code>/messages", views.messages, name="messages"),
    path("", views.chatrooms, name="chatrooms")
]
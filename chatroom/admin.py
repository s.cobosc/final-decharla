from django.contrib import admin
from .models import ChatRoom, ChatRoomMessage, Vote

admin.site.register(ChatRoom)
admin.site.register(ChatRoomMessage)
admin.site.register(Vote)
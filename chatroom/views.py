from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, JsonResponse
from .models import *
from django.views.decorators.csrf import csrf_exempt
import json
from django.core import serializers
from django.db import IntegrityError
from django.db.models.expressions import F
from .utils.parser import *
from xml.sax import *
import api.optional_api as api


def aemet_funcionality(request, chatroom, session):
    location_id = request.POST['location']

    #  location_id = '45161' # seseña
    location_data = api.get_time_aemet_by_location(location_id)
    location_data_filtered = api.filter_data(location_data)

    for message in location_data_filtered:
        chatroom_message = ChatRoomMessage(
            description=message, chatroom=chatroom, session=session)
        chatroom_message.save()
    print(location_data_filtered)


@csrf_exempt
def chatrooms(request):
    if request.method == 'GET':
        if 'search' in request.GET:
            search_input = request.GET['search']
            chatrooms = ChatRoom.objects.filter(name__icontains=search_input)
            chatrooms_list = serializers.serialize('json', chatrooms)
            return HttpResponse(json.dumps(chatrooms_list), content_type="application/json")
        elif 'format' in request.GET:
            format = request.GET['format']
            if format != 'json':
                response = HttpResponse('NOT FOUND')
                response.status_code = '404'
                return response
            # nothing happens... continue
            chatrooms = ChatRoom.objects.all()
            chatrooms_list = serializers.serialize('json', chatrooms)
            return HttpResponse(json.dumps(chatrooms_list), content_type="application/json")


@csrf_exempt
def chatroom(request, code):
    errors = {}
    if request.method == 'GET':
        chatroom = ChatRoom.objects.get(code=code)

        chatroom.messages = ChatRoomMessage.objects.filter(chatroom=chatroom)
        print(chatroom.__dict__)
        if chatroom == None:
            response = HttpResponse('NOT FOUND')
            response.status_code = '404'
            return response
        chatroom.likes = Vote.objects.filter(
            chatroom=chatroom, status=True).count()
        chatroom.dislikes = Vote.objects.filter(
            chatroom=chatroom, status=False).count()

        template = loader.get_template('chatroom/ChatRoomDetail.html')
        return HttpResponse(template.render({'css_properties': request.css_properties, 'chatroom': chatroom, 'path': request.path, 'metrics': request.metrics}))

    if request.method == 'POST':
        print('status' in request.POST)
        chatroom = ChatRoom.objects.get(code=code)

        # check session id on css_properties request
        session_id = request.css_properties['session_id']
        if session_id == None:
            response = HttpResponse('ERR INVALID SESSION')
            response.status_code = '404'
            return response
        session = Session.objects.get(id=session_id)

        if 'location' in request.POST:
            aemet_funcionality(request, chatroom, session)
        elif 'status' in request.POST:
            status_to_save = False
            if request.POST['status'] == 'true':
                status_to_save = True

            vote = Vote(status=status_to_save,
                        chatroom=chatroom, session=session)
            try:
                vote.save()
            except IntegrityError as e:
                errors['status_error'] = 'You already vote in this chatroom'
        else:
            # normal message into a chatroom by code
            message_to_save = ChatRoomMessage(
                description=request.POST['message'])
            if request.POST.get('is_image') == 'true':
                message_to_save.is_image = True
                message_to_save.image_url = request.POST['message']

            # TODO: VALIDATE QUERY PARAMS
            chatroom = ChatRoom.objects.get(code=code)
            if chatroom == None:
                response = HttpResponse('NOT FOUND')
                response.status_code = '404'
                return response

            message_to_save.session = session
            message_to_save.chatroom = chatroom
            message_to_save.save()

        chatroom.likes = Vote.objects.filter(
            chatroom=chatroom, status=True).count()
        chatroom.dislikes = Vote.objects.filter(
            chatroom=chatroom, status=False).count()
        chatroom.messages = ChatRoomMessage.objects.filter(chatroom=chatroom)

        template = loader.get_template('chatroom/ChatRoomDetail.html')

        return HttpResponse(template.render({'css_properties': request.css_properties, 'chatroom': chatroom, 'path': request.path, 'errors': errors, 'metrics': request.metrics}))


@csrf_exempt
def messages(request, code):
    if request.method == 'GET':
        if 'format' in request.GET:
            format = request.GET['format']
            if format != 'json':
                response = HttpResponse('NOT FOUND')
                response.status_code = '404'
                return response
            # nothing happens... continue
            chatroom = ChatRoom.objects.get(code=code)
            messages = ChatRoomMessage.objects.filter(chatroom=chatroom)
            messages_list = list(messages.values())
            for message in messages_list:
                if message['session_id'] == request.css_properties['session_id']:
                    message['self_message'] = True
                message['chatroom'] = list(ChatRoom.objects.filter(
                    id=message['chatroom_id']).values())[0]
                if message['session_id'] != None:
                    message['session'] = list(Session.objects.filter(id=message['session_id']).values())[0]
                else:
                    message['session'] = None

            return JsonResponse({"messages": messages_list}, json_dumps_params={'ensure_ascii': False}, status=201)

    if request.method == 'PUT':
        # XML PART

        messagesHandler = MessagesHandler()
        print(request.body)
        parseString(request.body.decode('utf-8'), messagesHandler)

        chatroom = ChatRoom.objects.get(code=code)
        for message in messagesHandler.message_list:
            message.chatroom = chatroom
            message.save()

        msg_list = serializers.serialize('json', messagesHandler.message_list)
        return HttpResponse('messages created', status=201)

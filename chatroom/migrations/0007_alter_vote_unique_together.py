# Generated by Django 4.1.7 on 2023-05-21 18:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('keeptalking', '0005_session_name'),
        ('chatroom', '0006_vote'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='vote',
            unique_together={('chatroom', 'session')},
        ),
    ]

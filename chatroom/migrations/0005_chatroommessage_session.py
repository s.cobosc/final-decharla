# Generated by Django 4.1.7 on 2023-05-19 17:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('keeptalking', '0003_session_created_at'),
        ('chatroom', '0004_alter_chatroommessage_image_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatroommessage',
            name='session',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.DO_NOTHING, to='keeptalking.session'),
        ),
    ]

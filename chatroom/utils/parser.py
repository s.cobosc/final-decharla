from xml.sax.handler import ContentHandler
from chatroom.models import *

class MessagesHandler(ContentHandler):

    def __init__ (self):
        self.CurrentData = ''
        self.inContent = False
        self.theContent = ""
        self.message_list = list()
        self.message = ChatRoomMessage()

    def startElement (self, name, attrs):
        if name == 'message':
            self.CurrentData = 'message'
            if attrs.get('isimg') == 'true':
                self.message.is_image = True
            elif attrs.get('isimg') == 'false':
                self.message.is_image = False
        elif name == 'text':
            self.CurrentData = 'text'
            self.inContent = True
            
    def endElement (self, name):
        if name == 'message':
            self.message_list.append(self.message)
            self.message = ChatRoomMessage()
        if self.inContent:
            self.inContent = False
            self.message.description = self.theContent.strip()
            if self.message.is_image:
                self.message.image_url = self.theContent.strip()
            self.theContent = ""
        
    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars
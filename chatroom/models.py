from django.db import models
from keeptalking.models import *

class ChatRoom(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    code = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class ChatRoomMessage(models.Model):
    description = models.CharField(max_length=500)
    is_image = models.BooleanField(default=False)
    image_url = models.URLField(default=None, null=True, blank=True)
    chatroom = models.ForeignKey(ChatRoom, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.DO_NOTHING, default=None, null=True)

class Vote(models.Model):
    status = models.BooleanField(default=True)
    chatroom = models.ForeignKey(ChatRoom, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.DO_NOTHING, default=None)

    class Meta:
        unique_together = ("chatroom", "session")
